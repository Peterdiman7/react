import { Link } from "react-router-dom";
import "../ReadyComponent/Ready.css";

const Ready = () => {
    return(
        <div className="main">
        <div className="bg">
                <div className="eclipse-one"></div>
                <div className="eclipse-two"></div>
            </div>
        <img className="image-ready" src="https://s3-alpha-sig.figma.com/img/68c6/588f/b0197d1a74427e73e08be18d6027d1de?Expires=1671408000&Signature=V3lICN0iaQLVgm8OS3cTUw~GRJO8llH1X1mow2-3TeoBXn-rFQnbDb~w77b2dYD~SOIlspTVbu5ArK~ZumKqv4A-dT1be61KwkWMn1N70RXLr7WHYuCT-FrUeDugTYuogJRUcEnzn9mQ4aaRXbbdjFws4nIprRMgOJz3Bgcd7031NB8hjTcgwAw0qstATRNO~TppJ9txwJjEom4gG7aWxoIKuTvrQJ0Hx0W9btAqCLgAebuUFNTyfTg2e~UZpDn7ebCgULBwbWr3tU-0JA5mP16Uvp9WdepgmeUuDVTdNAZTikixMCZqSGej2tPNQ6I39anxJc0HXl~i62-eop9DhA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" alt="img-ready" />

        <div className="desc-container-r">
            <span className="desc-r">
                <p className="desc-r-t">You are ready to go!</p>
                <p className="desc-r-d">Congratulation, any interesting topics will be shortly in your hands.</p>
            </span>
        </div>
        <div className="b-container">
            <button className="btn-r">
                <Link to="/l">
                    <p>Finish</p>
                </Link>
            </button>
        </div>



        <div className="bottom">
                <span className="bottom-bar">
                    <p className="indicator"></p>
                </span>
            </div>
        </div>
    );
}

export default Ready;