import "../TitleTwoComponent/TitleTwo.css";
import { Link } from "react-router-dom";

const TitleTwo = () => {
    return(
        <div>
            <section className="content">
                <div className="eclipse-container">
                    <p className="eclipse-one"></p>
                    <p className="eclipse-two"></p>
                    <p className="eclipse-three"></p>
                </div>
            <img className="image" src="https://s3-alpha-sig.figma.com/img/73b7/33e5/e71512c140d844ea295de99e5ef97622?Expires=1671408000&Signature=P34kUZrU6zv6AFS6Vinu6w~YlEQ7f~IupyBq~4K76zjqpV1bVnIRxy9hbT93hyQdKnIYRK5yqgeX~n1udzjhWAz26CpVi5~88aII4HozQecwgl285du9F2WhhnVjuPyS~cob9fj-UFl2Fyro4621p~POiUclPz4pQpPjqhY9rxJgr73NccMsjJccqb3AJHXKm0VtbLuCcIEdagSioaAz9RX3EzFWB2K8h67ZMdkBE0Wxctdt-dWDCF4cJVwBvZR3cvWSVbC0Ndjy-TLMtBMblkhk8Km67E0aWkzorfO-x8DoUbIIlg~AL1wNn9qwBON864tb4wmcGBuxiQb5ImlQIQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
            <span className="text">
                <h1 className="title-one">Title Two</h1>
                <p className="lorem">Lorem ipsum dolor sit amet la maryame dor sut colondeum.</p>
            </span>
            <div className="carrousel">
                <p className="dot-one-t2"></p>
                <p className="dot-two-t2"></p>
                <p className="dot-three-t2"></p>
            </div>
            
                <span className="button-skip">
                    <p>
                       <Link to={"/w"}>Skip</Link>
                    </p>
                </span>

                <span className="button-next">
                    <p>
                        <Link to={"/t3"}>Next</Link>
                    </p> 
                </span>

            </section>
            
            <span className="bottom-bar-title-two">
                <p className="indicator-title-two"></p>
            </span>
        </div>
    );
}

export default TitleTwo;