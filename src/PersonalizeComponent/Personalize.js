import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import ElementEight from "../CategoryElements/ElementEight";
import ElementEleven from "../CategoryElements/ElementEleven";
import ElementFive from "../CategoryElements/ElementFive";
import ElementFour from "../CategoryElements/ElementFour";
import ElementNine from "../CategoryElements/ElementNine";
import ElementOne from "../CategoryElements/ElementOne";
import ElementSeven from "../CategoryElements/ElementSeven";
import ElementSix from "../CategoryElements/ElementSix";
import ElementTen from "../CategoryElements/ElementTen";
import ElementThree from "../CategoryElements/ElementThree";
import ElementTwo from "../CategoryElements/ElementTwo";
import "../PersonalizeComponent/Personalize.css";

const Personalize = () => {

    const [isActive, setIsActive] = useState(false);
    const [count, setCount] = useState(0);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    return(
        <div>
                <div className="bg">
                    <p className="eclipse-one"></p>
                    <p className="eclipse-two"></p>
                </div>
                <article className="article-p">
                    <span className="span-p">
                        <p className="title-p">Personalize Suggestion</p>
                        <p className="desc-p">Choose <strong>min. 3 topics</strong> you like, we will give you more often that relate to it.</p>
                    </span>
                </article>
            <div className="topic-container">
                <input type="text" name="topic" className="topic" placeholder="Placeholder"/>
            </div>
            <p className="topics-count">{count} topics selected</p>
            
            <div className="categories">
                <span className="category-row-one">
                    <ElementOne />
                    <ElementTwo />
                    <ElementThree />
                </span>

                <span className="category-row-two">
                    <ElementFour />
                    <ElementFive />
                    <ElementSix />
                </span>
                <span className="category-row-four">
                    <ElementTen />
                    <ElementEleven />
                </span>

                <span className="category-row-three">
                    <ElementSeven />
                    <ElementEight />
                    <ElementNine />
                </span>
            </div>

            <div className="button-group">
                <button className="submit">
                    <Link to="/r">
                        <p>Submit</p>
                    </Link>
                </button>
                <button className="skip-btn">
                    <Link to="/r">
                        <p>Skip</p>
                    </Link>
                </button>
            </div>

            <span className="bottom-bar-title-one">
                <p className="indicator-title-one"></p>
            </span>
        </div>
    );
}

export default Personalize;