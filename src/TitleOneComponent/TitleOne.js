import "../TitleOneComponent/TitleOne.css";
import { Link } from "react-router-dom";
import TitleTwo from "../TitleTwoComponent/TitleTwo";

const TitleOne = () => {
    return(
        <div>
                <div className="bg">
                    <p className="eclipse-one"></p>
                    <p className="eclipse-two"></p>
                </div>
            <section className="content">
            <img className="image" src="https://s3-alpha-sig.figma.com/img/61db/f2ae/619f53de3c13ed00ee6db487fd386736?Expires=1671408000&Signature=dTGHUYKFqeGHpw20E6unnskMgDSopRhr~m8EnyZD~nXw4l92BfQO~CJokfcEN9N~GglGY5~1K-Ngh2o~QGGgFuPWEb7Ms0uVs5WxgvAeVNXihjsM7UsGmmbSd5v98BHSQvfn39V7Y9ovGegKyZfmeddOsMgE-1Fd0kT~a207uLX1KmEIL2GBZDEXVlNMJLsS7ALjBTnYlV99JtUxN3uBqFhwqpPg8bwZ85SfFEiLfVo2RAgZKPjjeUbZdDdQgBgQd6XG1-68OsVOwSGcu-rXpR39GHaz-XPQtZ5dXHcp~hi-IlPQ2ouT5n9OTgxPkfP9-Z7M8S-hpXpKwabt~E6Okw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
            <span className="text">
                <h1 className="title-one">Title One</h1>
                <p className="lorem">Lorem ipsum dolor sit amet la maryame dor sut colondeum.</p>
            </span>
            <div className="carrousel">
                <p className="dot-one-t1"></p>
                <p className="dot-two-t1"></p>
                <p className="dot-three-t1"></p>
            </div>
            
                <span className="button-skip">
                    <p>
                       <Link to={"/w"}>Skip</Link>
                    </p>
                </span>

                <span className="button-next">
                    <p>
                       <Link to={"/t2"}>Next</Link>
                    </p>
                </span>

            </section>
            
            <span className="bottom-bar-title-one">
                <p className="indicator-title-one"></p>
            </span>
        </div>
    );
}

export default TitleOne;