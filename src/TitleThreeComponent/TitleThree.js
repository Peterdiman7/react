import "../TitleThreeComponent/TitleThree.css";
import { Link } from "react-router-dom";

const TitleThree = () => {
    return(
        <div>
            <section className="content">
                <div className="eclipse-container">
                    <p className="eclipse-one"></p>
                    <p className="eclipse-two"></p>
                    <p className="eclipse-three"></p>
                </div>
            <img className="image" src="https://s3-alpha-sig.figma.com/img/f15e/c8e1/7254b15669f3373c7fbc1db50a62f546?Expires=1671408000&Signature=gwdRFiX85GY5UE0E89SwhKHHu3ljr3jxHFqHXYRs0cfWNFMULL9moz0pbEZNLsfgrpOGiZFhkkgGzuyVLQIcoIxwHFlLOo2TCXXhJsHOTQDiRNHTLiJz0VQcXIXcUdDF~6ja-JTE28FPWDjHdiz0X8bDngXc63YfRRfAOh4Z-U~tkVjrDGMoHWF3KBHSIqrzEuSVk44ek-EKVbwuaxExvG~Nmi1994gZflPdrQUaaWwgvIRAljaDsC4WkRd-x4d9MkG-orO4jHjWMtop8yxHB6yTRN55t6s43Okph1UnTpt28Xh6ZQuNnk9nXGO8XCx1JhBr~F51hx~tKHs7dHjWqg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
            <span className="text">
                <h1 className="title-one">Title Three</h1>
                <p className="lorem">Lorem ipsum dolor sit amet la maryame dor sut colondeum.</p>
            </span>
            <div className="carrousel">
                <p className="dot-one-t3"></p>
                <p className="dot-two-t3"></p>
                <p className="dot-three-t3"></p>
            </div>
            
                <span className="button-skip">
                    <p>
                       <Link to={"/w"}>Skip</Link>
                    </p>
                </span>

                <span className="button-next">
                   <p>
                   <Link to={"/w"}>Next</Link>
                    </p> 
                </span>

            </section>
            
            <span className="bottom-bar-title-three">
                <p className="indicator-title-three"></p>
            </span>
        </div>
    );
}

export default TitleThree;