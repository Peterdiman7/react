import "../WelcomePage/Welcome.css";
import { Link, redirect } from "react-router-dom";

const WelcomePage = () => {

    const goToPersonalize = () => {
        if(true){
            redirect("/p");
        }
    }

    return(
        <div className="welcome-container">
            <div className="bg">
                <div className="eclipse-one"></div>
                <div className="eclipse-two"></div>
            </div>
            <h1 className="welcome-title">Welcome!</h1>
            <h1 className="desc">Find what you are looking for</h1>
            <p className="more-desc">By personalize your account, we can help you to find what you like.</p>
            <div className="buttons">
                <div className="personalize">
                    <Link to="/p">
                <button className="personalize-btn">Personalize</button>
                    </Link>
                </div>
                    <div className="skip">
                    <Link to="/r">
                        <button className="skip-btn">Skip</button>
                    </Link>
                    </div>
            </div>

            <div className="bottom">
                <span className="bottom-bar">
                    <p className="indicator"></p>
                </span>
            </div>

        </div>
    );
}

export default WelcomePage;