import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import styles from "../PlayerComponent/Player.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { faCirclePlay } from "@fortawesome/free-solid-svg-icons";

const Player = () => {

    let navigate = useNavigate();
    const goBackLib = () => {
        let path = '/l';
        navigate(path);
    }
    const { id } = useParams();
    const [book, setBook] = useState([]);

    useEffect(() => fetch("http://localhost:3000/books/" + id)
    .then(res => res.json())
    .then(data => setBook(data)), []);

    return(
        <div>
            <header className={styles.header}>
                <span className={styles.dots}>
                <article className={styles.dotcontainer}>
                    <span className={styles.moredots}>
                        <p className={styles.dotone}></p>
                        <p className={styles.dottwo}></p>
                        <p className={styles.dotthree}></p>
                    </span>
                </article>
                </span>
                <p className={styles.title}>{ book.title }</p>
                <article className={styles.iconcontainer}>
                <FontAwesomeIcon onClick={goBackLib} icon={faChevronLeft} color="white" />
                </article>
            </header>

            <span className={styles.cover}>
                <div className={styles.imgholder}>
                    <img src={book.img} alt="Image of the book..." />
                </div>
            </span>
            <div className={styles.booktitle}>{book.title}</div>
            <div className={styles.authorname}>{book.author}</div>


            <article className={styles.timelinecontainer}>
                <p className={styles.linecontainer}></p>
                <p className={styles.line}></p>
                <p className={styles.dot}></p>
                <p className={styles.textone}>12:50</p>
                <p className={styles.texttwo}>48:38</p>
            </article>
            <article className={styles.buttoncontainer}>
                <span className={styles.buttonposition}>
                    <Link to={"/l"}>
                    <FontAwesomeIcon icon={faCirclePlay} size="4x" color="white" />
                    </Link>
                </span>
            </article>

            <div className="bottom">
                <span className="bottom-bar">
                    <p className="indicator"></p>
                </span>
            </div>
        </div>
    );
}

export default Player;