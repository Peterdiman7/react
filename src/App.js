import "./App.css";
import TitleOne from "./TitleOneComponent/TitleOne";
import TitleThree from "./TitleThreeComponent/TitleThree";
import TitleTwo from "./TitleTwoComponent/TitleTwo";
import WelcomePage from "./WelcomePage/Welcome";
import { Routes, Route, Router } from "react-router-dom";
import Personalize from "./PersonalizeComponent/Personalize";
import Ready from "./ReadyComponent/Ready";
import Library from "./LibraryComponent/Library";
import Player from "./PlayerComponent/Player";
function App() {
  return (
    <div className="App">
      <Routes>
          <Route path="/" element={<TitleOne />} />
          <Route path="t2" element={<TitleTwo />} />
          <Route path="/t3" element={<TitleThree />} />
          <Route path="/w" element={<WelcomePage />} />
          <Route path="/p" element={<Personalize />} />
          <Route path="r" element={<Ready />} />
          <Route path="l" element={<Library />} />
          {/* <Route path="pl" element={<Player />} /> */}

          <Route path="/l/:id" element={<Player />}/>
      </Routes>
    </div>
  );
}

export default App;
