import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "../LibraryComponent/Library.css";

const Library = () => {

  const [query, setQuery] = useState("");
  
  const [books, setBooks] = useState([]);

  useEffect(() => fetch("http://localhost:3000/books")
    .then(res => res.json())
    .then(data => setBooks(data)), []);
    
    return (
      <div>
      <article className="header-lib">
        <img
          className="bookly-logo"
          src="https://s3-alpha-sig.figma.com/img/a9bf/da77/827410f8adf948f5c992934999c79ebc?Expires=1671408000&Signature=BBcu1MorTysEVzGN0933hI0nWH41V-l8Vm~wJzxp6jmeB3Ob9rAPu5hGOoybyDED~VX1FLX~-ocGNAjE2hc9fK5ph3szhCo9mA-es9IaHqpcsM8HDkNgj8vz9I0LisBNSV7YAvTzFq6luQ-yyY2kwsFCVmldkw8zZjrGa7K3MFMnzuv3D6WkJ0TkrZbvzJr56RAA9Bq0HdeVAN38Dnvf8hHiyHqx0eVAbWc4KuMSOcCKaxFAzn27ZIUrfx3Y6KhGslTaf1h-hOg~coevOGNKSrhs4McFXRq6xilsqvV3blTuQVM72bKZT~yIMzs270Pms4jzV~sjS1J8g~0ItMnF8Q__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
          alt="bookLy"
        />
        <p className="bookly-text">Bookly</p>
      </article>

      <div className="my-books">
        <p>My books</p>
        <span className="search">
          <input
          onChange={e => setQuery(e.target.value)}
            className="search-input"
            type="text"
            name="search"
            placeholder="Search Books or Author..."
          />
        </span>
      </div>

      <div className="my-books-list">
        {books.filter((book) => 
        book.title.toLowerCase().includes(query)
        ).map((book) => (
        <div className="card-bestseller-one">
          <div className="cardspan-one">
            <div className="img-placeholder-one">
              <Link to={`/l/${book.id}`}>
              <img
                className="img-one"
                src={book.img}
                alt="book-image"
                />
              </Link>
            </div>
            <span className="card-text-one">
              <div className="card-text-container">
                <p className="t1">{book.title}</p>
                <p className="desc1">{book.author}</p>
              </div>
            </span>
          </div>
        </div>))}
      
      <span className="bottom-bar-title-one">
        <p className="indicator-title-one"></p>
      </span>
    </div>
  </div>
  );
};
export default Library;
