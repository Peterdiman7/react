import { useState } from "react";


const ElementSeven = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-seven${toggleClassCheck}`} onClick={handleClick}>
            <p>News</p>
        </div>
    );
}

export default ElementSeven;