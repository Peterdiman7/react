import { useState } from "react";


const ElementSix = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-six${toggleClassCheck}`} onClick={handleClick}>
            <p>Education</p>
        </div>
    );
}

export default ElementSix;