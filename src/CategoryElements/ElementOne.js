import { useState } from "react";


const ElementOne = () => {

    const [isActive, setIsActive] = useState(false);
    const [count, setCount] = useState(0);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div name="art" className={`category-element-one${toggleClassCheck}`} onClick={handleClick}>
            <p>Art</p>
        </div>
    );
}

export default ElementOne;