import { useState } from "react";


const ElementThree = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-three${toggleClassCheck}`} onClick={handleClick}>
            <p>Biography</p>
        </div>
    );
}

export default ElementThree;