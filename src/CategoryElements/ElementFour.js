import { useState } from "react";


const ElementFour = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-four${toggleClassCheck}`} onClick={handleClick}>
            <p>Comedy</p>
        </div>
    );
}

export default ElementFour;