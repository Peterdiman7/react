import { useState } from "react";


const ElementTen = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';
    return(
        <div className={`category-element-ten${toggleClassCheck}`} onClick={handleClick}>
            <p>Technology</p>
        </div>
    );
}

export default ElementTen;