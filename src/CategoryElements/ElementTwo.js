import { useState } from "react";


const ElementTwo = () => {

    const [isActive, setIsActive] = useState(false);
    const [count, setCount] = useState(0);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div name="business" className={`category-element-two${toggleClassCheck}`} onClick={handleClick}>
            <p>Business</p>
        </div>
    );
}

export default ElementTwo;