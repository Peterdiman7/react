import { useState } from "react";


const ElementFive = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-five${toggleClassCheck}`} onClick={handleClick}>
            <p>Culture</p>
        </div>
    );
}

export default ElementFive;