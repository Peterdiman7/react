import { useState } from "react";


const ElementEight = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';

    return(
        <div className={`category-element-eight${toggleClassCheck}`} onClick={handleClick}>
            <p>Philosophy</p>
        </div>
    );
}

export default ElementEight;