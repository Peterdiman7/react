import { useState } from "react";


const ElementEleven = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';
    return(
        <div className={`category-element-eleven${toggleClassCheck}`} onClick={handleClick}>
            <p>Travel</p>
        </div>
    );
}

export default ElementEleven;