import { useState } from "react";


const ElementNine = () => {

    const [isActive, setIsActive] = useState(false);
    function handleClick(e) {
        setIsActive(current => !current);
        }

    let toggleClassCheck = isActive ? ' active' : '';
    return(
        <div className={`category-element-nine${toggleClassCheck}`} onClick={handleClick}>
            <p>Psychology</p>
        </div>
    );
}

export default ElementNine;